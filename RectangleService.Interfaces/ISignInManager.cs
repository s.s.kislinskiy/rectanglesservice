﻿

using RectangleService.Models.RequestModels;

namespace RectangleService.Interfaces
{
    public interface ISignInManager
    {
        Task<bool> CheckIfUserExistsAsync(string username);
        Task<bool> PasswordSignInAsync(LoginModel model);
    }
}