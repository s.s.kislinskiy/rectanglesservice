﻿using RectangleService.Models.DAL;
using RectangleService.Models.RequestModels;

namespace RectangleService.Interfaces
{
    public interface IRectangleRetriever
    {
        Task<Dictionary<string, List<Rectangle>>> GetMatchedRectangles(Coordinate[] coordinates);
    }
}