﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace RectangleService.Models.DAL;

public partial class GeoDbContext : DbContext
{
    public GeoDbContext()
    {
    }

    public GeoDbContext(DbContextOptions<GeoDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Rectangle> Rectangles { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .HasPostgresExtension("pg_catalog", "adminpack")
            .HasPostgresExtension("pgagent", "pgagent");

        modelBuilder.Entity<Rectangle>(entity =>
        {
            entity
                .ToTable("rectangles");

            entity.Property(e => e.Height).HasColumnName("height");
            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.LeftTopXCoord).HasColumnName("left_top_x_coord");
            entity.Property(e => e.LeftTopYCoord).HasColumnName("left_top_y_coord");
            entity.Property(e => e.Width).HasColumnName("width");
            entity.HasKey(entity => entity.Id);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
