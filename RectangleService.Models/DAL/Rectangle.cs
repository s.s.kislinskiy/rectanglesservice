﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RectangleService.Models.DAL;

public partial class Rectangle
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    public double LeftTopYCoord { get; set; }

    public double LeftTopXCoord { get; set; }

    public double Width { get; set; }

    public double Height { get; set; }
}
