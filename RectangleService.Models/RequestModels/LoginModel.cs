﻿using System.ComponentModel.DataAnnotations;

namespace RectangleService.Models.RequestModels
{
    public class LoginModel
    {
        [Required]
        [DataType(DataType.Text)]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
