﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Http.ModelBinding;

namespace RectangleService.Models.RequestModels
{
    public class Coordinate
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Coordinate(double x, double y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"{X},{Y}";
        }


    }
}
