﻿
using global::RectangleService.Models.DAL;
using global::RectangleService.Models.RequestModels;
using NUnit.Framework;
using RectangleService.BL.Helpers;


namespace RectangleService.Tests
{
        [TestFixture]
        public class RectangleExtensionsTests
        {
            [Test]
            public void Contains_ReturnsTrue_WhenCoordinateIsWithinRectangle()
            {
                Rectangle rectangle = new Rectangle
                {
                    LeftTopXCoord = 1,
                    LeftTopYCoord = 2,
                    Width = 3,
                    Height = 4
                };

                Coordinate coordinate = new Coordinate(1.5, 1);
                bool contains = rectangle.Contains(coordinate);
                Assert.That(contains, Is.True);
            }

            [Test]
            public void Contains_ReturnsFalse_WhenCoordinateIsNotWithinRectangle()
            {
                Rectangle rectangle = new Rectangle
                {
                    LeftTopXCoord = 1,
                    LeftTopYCoord = 2,
                    Width = 3,
                    Height = 4
                };

                Coordinate coordinate = new Coordinate(5, 6);
                bool contains = rectangle.Contains(coordinate);
                Assert.That(contains, Is.False);
            }

            [Test]
            public void Contains_ReturnsFalse_WhenRectangleIsNull()
            {
                Rectangle rectangle = null;

                Coordinate coordinate = new Coordinate(1, 2);
                bool contains = rectangle.Contains(coordinate);
                Assert.That(contains, Is.False);
            }
        }
    }