using RectangleService.BL.Services;
using RectangleService.Models.DAL;
using RectangleService.Models.RequestModels;

namespace RectangleService.Tests
{
    [TestFixture]
    public class RectangleRetrieverTests
    {
        [Test]
        public async Task MatchedRectangles_ShouldReturnMatchingRectangles()
        {
            // Arrange
            var coordinates = new[]
            {
               new Coordinate(2, -2),
               new Coordinate(7, 7)
            };

            var mockDbContext = CreateMockDbContextWithRectangles();
            mockDbContext.Rectangles.AddRange(
                 new Rectangle { LeftTopXCoord = 0, LeftTopYCoord = 0, Width = 5, Height = 5 },
                 new Rectangle { LeftTopXCoord = 10, LeftTopYCoord = 10, Width = 5, Height = 5 }
            );

            mockDbContext.SaveChanges();


            var retriever = new RectangleRetriever(mockDbContext);

            // Act
            Dictionary<string, List<Rectangle>> result = await retriever.GetMatchedRectangles(coordinates);

            // Assert
            Assert.That(result.Count, Is.EqualTo(2));
            Assert.That(result[coordinates[0].ToString()].Count, Is.EqualTo(1));
            Assert.That(result[coordinates[1].ToString()].Count, Is.EqualTo(0));
        }

        private GeoDbContext CreateMockDbContextWithRectangles()
        {
            return MockGeoDbContext.CreateMockDbContext();
        }
    }
}