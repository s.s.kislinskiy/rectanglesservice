﻿using Microsoft.EntityFrameworkCore;
using RectangleService.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RectangleService.Tests
{
    public class MockGeoDbContext : GeoDbContext
    {
        private MockGeoDbContext(DbContextOptions<GeoDbContext> options)
            : base(options)
        {
        }

        public static MockGeoDbContext CreateMockDbContext()
        {
            var options = new DbContextOptionsBuilder<GeoDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var dbContext = new MockGeoDbContext(options);

            return dbContext;
        }
    }
}
