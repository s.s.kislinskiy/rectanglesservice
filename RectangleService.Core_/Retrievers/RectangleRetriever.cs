﻿using Microsoft.EntityFrameworkCore;
using RectangleService.BL.Helpers;
using RectangleService.Interfaces;
using RectangleService.Models.DAL;
using RectangleService.Models.RequestModels;

namespace RectangleService.BL.Services
{
    public class RectangleRetriever : IRectangleRetriever
    {
        private readonly GeoDbContext context;

        public RectangleRetriever(GeoDbContext context)
        {
            this.context = context;
        }

        public async Task<Dictionary<string, List<Rectangle>>> GetMatchedRectangles(Coordinate[] coords)
        {
            Rectangle[] rectangles = await context.Rectangles.AsNoTracking().ToArrayAsync();

            var matchingRectangles = new Dictionary<string, List<Rectangle>>();

            foreach (var coordinate in coords)
            {
                //here I just fill firstly all empty lists
                matchingRectangles.Add(coordinate.ToString(), new List<Rectangle>());

                foreach (var rectangle in rectangles)
                {
                    if (rectangle.Contains(coordinate))
                    {
                        matchingRectangles[coordinate.ToString()].Add(rectangle);
                    }
                }
            }

            return matchingRectangles;
        }
    }
}
