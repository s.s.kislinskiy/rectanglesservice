﻿using RectangleService.Models.DAL;
using RectangleService.Models.RequestModels;

namespace RectangleService.BL.Helpers
{
    public static class RectangleExtensions
    {
        public static bool Contains(this Rectangle rectangle, Coordinate coordinate)
        {
            if (rectangle == null)
            {
                return false;
            }

            if (coordinate.X >= rectangle.LeftTopXCoord && coordinate.X <= rectangle.LeftTopXCoord + rectangle.Width &&
               coordinate.Y <= rectangle.LeftTopYCoord && coordinate.Y >= rectangle.LeftTopYCoord - rectangle.Height)
            {
                return true;
            }

            return false;
        }
    }
}
