﻿using RectangleService.Models.DAL;

namespace RectangleService.BL.Helpers
{
    public static class RectangleGenerator
    {
        static int DefaultCount = 200;
        public static void CreateNewRandomRectangles(GeoDbContext context, int? count)
        {
            var rectangles = new List<Rectangle>();
            for (int i = 0; i < (count ?? DefaultCount); i++)
            {
                var rectangle = new Rectangle()
                {
                    LeftTopXCoord = Random.Shared.Next(300),
                    LeftTopYCoord = Random.Shared.Next(300),
                    Width = Random.Shared.Next(10, 100),
                    Height = Random.Shared.Next(10, 100)
                };

                rectangles.Add(rectangle);
            }

            context.Rectangles.AddRange(rectangles);
            context.SaveChanges();
        }
    }
}
