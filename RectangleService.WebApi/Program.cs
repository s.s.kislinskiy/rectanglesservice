using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RectangleService.Api.Authentification;
using RectangleService.BL.Helpers;
using RectangleService.BL.Managers;
using RectangleService.BL.Services;
using RectangleService.Interfaces;
using RectangleService.Models.DAL;
using RectangleService.WebApi.ModelBinders;
using System;
using System.Reflection;
using System.Text;

namespace RectangleService.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false).Build();

            // Add services to the container.
            SetupAuth(builder);

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Rectangle Service API",
                    Description = "Geomentry api",
                });
                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename),true);
            });
            SetupDependencies(builder, configuration);

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthentication();
            app.UseAuthorization();
            app.MapControllers();
            app.Run();
        }

        private static void SetupDependencies(WebApplicationBuilder builder, IConfigurationRoot configuration)
        {
            builder.Services.AddDbContext<GeoDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("GeoDbConnectionString")));

            builder.Services.AddScoped<IRectangleRetriever, RectangleRetriever>();
            builder.Services.AddScoped<ISignInManager, SignInManager>();
        }

        private static void SetupAuth(WebApplicationBuilder builder)
        {
            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,               // Validate the JWT issuer
                    ValidateAudience = true,             // Validate the audience
                    ValidateLifetime = true,             // Check the token expiration
                    ValidateIssuerSigningKey = true,     // Validate the signing key
                    ValidIssuer = AuthOptions.ISSUER,        // Replace with your issuer
                    ValidAudience = AuthOptions.AUDIENCE,    // Replace with your audience
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AuthOptions.KEY))
                };
            });
        }
    }
}