using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.SqlServer.Server;
using RectangleService.BL.Helpers;
using RectangleService.Interfaces;
using RectangleService.Models.DAL;
using RectangleService.Models.RequestModels;
using RectangleService.WebApi.ModelBinders;
using Swashbuckle.AspNetCore.Annotations;

namespace RectangleService.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class GeometryController : ControllerBase
    {
        private IRectangleRetriever _rectangleRetriever;
        private GeoDbContext _geoDbContext;

        public GeometryController(IRectangleRetriever rectangleRetriever, GeoDbContext geoDbContext)
        {
            _rectangleRetriever = rectangleRetriever;
            _geoDbContext = geoDbContext;
        }

        /// <summary>
        /// Retrieves matched rectangles by coords
        /// </summary>
        /// <param name="coords">query string array of coords in URL format for using:{apihost}/rectangles?coords=0,0&amp;coords=1,1&amp;coords=2,2</param>
        /// <remarks> 
        /// returns such example of json
        /// {
        ///"65,170": [
        ///  {
        ///    "id": 2,
        ///    "leftTopYCoord": 177,
        ///    "leftTopXCoord": 64,
        ///    "width": 50,
        ///    "height": 45
        ///  },
        ///  {
        ///    "id": 3,
        ///    "leftTopYCoord": 190,
        ///    "leftTopXCoord": 41,
        ///    "width": 62,
        ///    "height": 59
        ///  },
        ///  "0,11": []
        /// </remarks>        
        [HttpGet("/rectangles")]
        public async Task<IActionResult> GetMatchedRectangles(
            [FromQuery] string[] coords)
        {
            Coordinate[] coordsObjList;
            try
            {
                coordsObjList = CoordinateCustomParser.Parse(coords);
            }
            catch
            {
                return BadRequest("Valid request must be in " +
                    "format: http://{apihost}/rectangles?coords=0,0&coords=1,1&coords=2,2");
            }
        
            return Ok(await _rectangleRetriever.GetMatchedRectangles(coordsObjList));
        }

        /// <summary>
        /// Create new random rectangles
        /// </summary>
        /// <param name="count">count of new rectangles</param>
        /// 
        [HttpPost("/rectangles/random")]
        public IActionResult CreateRandomRectangles([FromQuery] int? count)
        {
            RectangleGenerator.CreateNewRandomRectangles(_geoDbContext, count);
            return Ok();
        }
    }
}