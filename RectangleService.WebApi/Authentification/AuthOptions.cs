﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace RectangleService.Api.Authentification
{
    public class AuthOptions
    {
        public readonly static string ISSUER = "MyAuthServer";
        public readonly static string AUDIENCE = "MyAuthClient"; 
        public readonly static string KEY = "secretkey24224243353463645634643634643643346634";   
    }
}
