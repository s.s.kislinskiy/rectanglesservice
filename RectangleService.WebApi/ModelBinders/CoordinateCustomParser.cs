﻿using RectangleService.Models.RequestModels;

namespace RectangleService.WebApi.ModelBinders
{
    public static class CoordinateCustomParser
    {
        public static Coordinate[] Parse(string[] coords)
        {
            var parsedObjList = new Coordinate[coords.Length];
            int i = 0;
            foreach (var coord in coords)
            {
                var splittedValues = coord.Split(',');
                var coordObj = new Coordinate(double.Parse(splittedValues[0]), double.Parse(splittedValues[1]));
                parsedObjList[i] = coordObj;
                i++;  
            }
            return parsedObjList;
        }
    }
}
